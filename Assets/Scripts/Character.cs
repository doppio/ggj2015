﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour
{
	public string Name;
	public Sprite ExorcisedSprite;
	public SpriteRenderer Sprite;
	public GameObject DeathFx;
	public GameObject SpeechBubbleAnchor;
	public AudioClip Voice;

	private bool m_IsAlive = true;
	private ConversationNode m_CurrentConversation;

	public ConversationNode StartingConvo;

	public ConversationNode[] ConversationRounds;

	public void SayStartingConvo()
	{
		if(StartingConvo)
		{
			StartCoroutine(DoStartingConvoSequence());
		}
	}

	private IEnumerator DoStartingConvoSequence()
	{
		UISpeechBubble.Instance.LoadNode(this, StartingConvo);
		yield return null;
		yield return null;
		yield return null;
		UISpeechBubble.Instance.BoxStretcher.Refresh();
	}

	public void LoadRound(int round)
	{
		m_CurrentConversation = ConversationRounds[round];
	}

	public void OnMouseUp()
	{
		if(GameManager.Instance.InteractionsPermitted && m_IsAlive)
		{
			if(UISpeechBubble.Instance.CurrentSpeaker == this)
				UISpeechBubble.Instance.Hide ();
			else
			{
				UISpeechBubble.Instance.LoadNode(this, m_CurrentConversation);
			}
		}
	}

	public void Die()
	{
		m_IsAlive = false;
		DeathFx.gameObject.SetActive(true);
	}

	public void Exorcise()
	{
		m_IsAlive = false;
		Sprite.sprite = ExorcisedSprite;
	}

	public bool IsPossessed
	{
		get
		{
			return GameManager.Instance.PossessedCharacter == this;
		}
	}
}
