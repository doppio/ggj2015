﻿using UnityEngine;
using System.Collections;

public class SpeedyAnimation : MonoBehaviour {
	private Animator m_Animator;

	void Awake()
	{
		m_Animator = GetComponent<Animator>();
	}

	void Update()
	{
		m_Animator.speed = ScrollingBackground.SpeedMultiplier * 0.3f;
	}
}
