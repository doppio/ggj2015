﻿using UnityEngine;
using System.Collections;

public class UIConversationChoice : MonoBehaviour {
	public UILabel Text;

	private ConversationChoice m_Choice;
	public ConversationChoice Choice
	{
		get { return m_Choice; }
		set
		{
			m_Choice = value;
			m_ContinueNode = null;
			Text.text = "> " + m_Choice.Response;
		}
	}

	public ConversationNode m_ContinueNode;
	public ConversationNode ContinueNode
	{
		get { return m_ContinueNode; }
		set
		{
			m_Choice = null;
			m_ContinueNode = value;
			Text.text = "> [Continue]";
		}
	}

	public Character ContinueSpeaker { get; set; }

	public void ChoiceSelected()
	{
		if(ContinueNode)
		{
			UISpeechBubble.Instance.LoadNode(ContinueSpeaker, ContinueNode);
			return;
		}

		if(Choice.IsAccusing)
			GameManager.Instance.Accuse();
		else
		{
			Character speakerInstance = null;
			if(!string.IsNullOrEmpty(Choice.SpeakerName))
				speakerInstance = GameManager.Instance.GetCharacterByName(Choice.SpeakerName);
			
			if(speakerInstance || string.IsNullOrEmpty(Choice.SpeakerName))
			{
				if(Choice != null)
					UISpeechBubble.Instance.LoadNode(speakerInstance, Choice.NextNode);
			}
		}
	}
}
