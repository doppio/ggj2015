﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UISpeechBubble : MonoBehaviour
{
	private static UISpeechBubble m_Instance;
	public static UISpeechBubble Instance
	{
		get { return m_Instance; }
	}

	public GameObject Content;
	public GameObject PointerAnchor;
	public UILabel SpeakerName;
	public UILabel Text;
	public UITable ConversationChoices;
	public UIConversationChoice ChoiceTemplate;
	public UIStretchToContents BoxStretcher;

	private List<UIConversationChoice> m_ConversationChoices = new List<UIConversationChoice>();

	public Character CurrentSpeaker { get; set; }

	void Awake()
	{
		m_Instance = this;
	}

	void Start()
	{
		m_ConversationChoices.Add(ChoiceTemplate);
		ChoiceTemplate.gameObject.SetActive(false);
	}

	void Update()
	{
		if(CurrentSpeaker)
		{
			UpdatePosition(true);
			UpdateAnchors();
		}
	}

	public void Hide()
	{
		Content.SetActive(false);
		CurrentSpeaker = null;
	}

	public void LoadNode(Character speaker, ConversationNode node)
	{
		if(node == null)
		{
			Hide ();
			return;
		}

		if(speaker == null)
			speaker = CurrentSpeaker;
		if(speaker != CurrentSpeaker)
		{
			CurrentSpeaker = speaker;
			if(CurrentSpeaker.Voice)
				AudioPlayer.Instance.Play (CurrentSpeaker.Voice);
		}

		Content.SetActive(true);

		SpeakerName.text = speaker.Name;
		string text = node.GetText(speaker);
		text = text.Replace ("  ", " ");
		Text.text = text;
		Text.fontStyle = node.Italicized ? FontStyle.Italic : FontStyle.Normal;

		int numResponses = node.Responses.Length;
		if(node.Responses.Length > 0)
		{
			for(int i = 0; i < node.Responses.Length; ++i)
			{
				UIConversationChoice choice = GetConversationChoice(i);
				choice.Choice = node.Responses[i];
			}
		}
		else
		{
			UIConversationChoice choice = GetConversationChoice(0);
			choice.ContinueNode = node.ContinueNode;
			choice.ContinueSpeaker = GameManager.Instance.GetCharacterByName(node.ContinueCharacterName);
			numResponses = 1;
		}

		// Deactivate extras
		for(int i = numResponses; i < m_ConversationChoices.Count; ++i)
			m_ConversationChoices[i].gameObject.SetActive(false);

		UpdateAnchors();

		ConversationChoices.Reposition();
		BoxStretcher.Refresh ();
		BoxStretcher.RefreshNow = true;

		UpdateAnchors();

		UpdatePosition();

		UpdateAnchors();
	}

	private void UpdatePosition(bool ignoreYPosition = false)
	{
		if(!CurrentSpeaker) return;

		Vector3 position = UICamera.FindCameraForLayer(gameObject.layer).camera.ScreenToWorldPoint(Camera.main.WorldToScreenPoint(CurrentSpeaker.SpeechBubbleAnchor.transform.position));
		position.z = 0f;
		if(ignoreYPosition)
			position.y = PointerAnchor.transform.position.y;
		PointerAnchor.transform.position = position;
	}

	private void UpdateAnchors()
	{
		// Hackety hack hack. Avert your eyes from this monstrosity.
		for(int i = 0; i < 2; ++i)
		{
			foreach(UIAnchor anchor in transform.GetComponentsInChildren<UIAnchor>(true))
				anchor.Update();
		}
	}

	private UIConversationChoice GetConversationChoice(int index)
	{
		UIConversationChoice conversationChoice = null;

		if(index >= m_ConversationChoices.Count)
		{
			GameObject newChild = NGUITools.AddChild(ConversationChoices.gameObject, ChoiceTemplate.gameObject);
			conversationChoice = newChild.GetComponent<UIConversationChoice>();
			m_ConversationChoices.Add(conversationChoice);
		}
		else
		{
			conversationChoice = m_ConversationChoices[index];
		}

		conversationChoice.gameObject.SetActive(true);

		return conversationChoice;
	}
}
