﻿using UnityEngine;
using System.Collections;

public class ConversationNode : MonoBehaviour {
	public string NormalText;
	public string PossessedText;
	public ConversationChoice[] Responses;
	public ConversationNode ContinueNode;
	public string ContinueCharacterName;
	public bool Italicized;

	public string GetText(Character speaker)
	{
		return ((speaker.IsPossessed && !string.IsNullOrEmpty(PossessedText)) ? PossessedText : NormalText);
	}
}
