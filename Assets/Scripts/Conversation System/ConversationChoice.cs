﻿using UnityEngine;
using System.Collections;

public class ConversationChoice : MonoBehaviour {
	public string Response;
	public string SpeakerName;
	public ConversationNode NextNode;
	public bool IsAccusing;
}
