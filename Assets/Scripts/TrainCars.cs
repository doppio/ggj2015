﻿using UnityEngine;
using System.Collections;

public class TrainCars : MonoBehaviour
{
	public float Speed;
	public float MinX;
	public float MaxX;

	public GameObject TrainDragger;

	private bool m_MovingOffScreen;
	public bool MovingOffScreen { get { return m_MovingOffScreen;  } }

	void Update()
	{
		if(m_MovingOffScreen)
			transform.Translate (Vector3.right * Speed * Time.unscaledDeltaTime, Space.World);
		else
		{
			Vector3 position = transform.position;
			position.x = Mathf.Clamp (position.x, MinX, MaxX);
			transform.position = position;
		}
	}


	public void Stop()
	{
		TrainDragger.gameObject.SetActive(false);
	}

	public void GoOffScreen()
	{
		m_MovingOffScreen = true;
		TrainDragger.gameObject.SetActive(false);
	}
}
