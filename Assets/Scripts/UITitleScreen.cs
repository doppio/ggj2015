﻿using UnityEngine;
using System.Collections;

public class UITitleScreen : MonoBehaviour
{
	void OnClick()
	{
		collider.enabled = false;
		GetComponent<TweenAlpha>().PlayForward();
	}

	public void Hide()
	{
		GameManager.Instance.StartGame();
		gameObject.SetActive(false);
	}
}
