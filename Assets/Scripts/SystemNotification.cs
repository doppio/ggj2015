﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SystemNotification : MonoBehaviour {
	private static SystemNotification m_Instance;
	public static SystemNotification Instance
	{
		get { return m_Instance; }
	}
	
	public UILabel Text;
	public Color CharacterNameColor;

	public struct Notification
	{
		public string Message;
		public float Duration;
	}

	public Queue<Notification> Notifications = new Queue<Notification>();

	private TweenAlpha m_AlphaTween;
	private bool m_Showing;
	private bool m_Hiding;
	public bool Showing { get { return m_Showing; } }

	void Awake()
	{
		m_Instance = this;
		m_AlphaTween = GetComponent<TweenAlpha>();
	}

	void Update()
	{
		if(Input.GetMouseButtonUp(0))
		{
			if(m_Showing && !m_Hiding)
			{
				StopAllCoroutines();
				if(Notifications.Count > 0)
				{
					ShowNextMessage();
				}
				else
				{
					StartCoroutine(Hide());
				}
			}
		}
	}

	public void PushMessage(string text, float duration = 4f)
	{
		if(!m_Showing)
		{
			StartCoroutine(Show (text, duration));
		}
		else
		{
			Notification newNotification;
			newNotification.Message = text;
			newNotification.Duration = duration;
			Notifications.Enqueue(newNotification);
		}
	}

	private void ShowNextMessage()
	{
		Notification notification = Notifications.Dequeue();
		StartCoroutine(Show (notification.Message, notification.Duration));
	}

	private IEnumerator Hide()
	{
		m_Hiding = true;
		m_AlphaTween.PlayReverse();
		
		yield return new WaitForSeconds(m_AlphaTween.duration);
		
		m_Hiding = false;
		m_Showing = false;
	}

	private IEnumerator Show(string text, float duration)
	{
		m_Showing = true;
		Text.text = text;
		m_AlphaTween.PlayForward();
		yield return new WaitForSeconds(m_AlphaTween.duration + duration);

		yield return StartCoroutine(Hide());

		if(Notifications.Count > 0)
		{
			Notification notification = Notifications.Dequeue();
			StartCoroutine(Show (notification.Message, notification.Duration));
		}	
	}
}
