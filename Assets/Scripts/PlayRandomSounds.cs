﻿using UnityEngine;
using System.Collections;

public class PlayRandomSounds : MonoBehaviour
{
	public AudioClip[] Clips;
	public int MinTime;
	public int MaxTime;

	private float m_NextTime;
	private AudioClip m_NextClip;

	void Start()
	{
		ChooseNextTime();
	}

	void Update()
	{
		if(Time.realtimeSinceStartup >= m_NextTime)
		{
			audio.PlayOneShot(m_NextClip);
			ChooseNextTime();
		}
	}

	private void ChooseNextTime()
	{
		m_NextTime = Time.realtimeSinceStartup + Random.Range(MinTime, MaxTime);
		if(m_NextClip)
			m_NextTime += m_NextClip.length;

		m_NextClip = Clips[Random.Range (0, Clips.Length - 1)];
	}
}
