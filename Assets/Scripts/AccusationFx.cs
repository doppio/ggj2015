﻿using UnityEngine;
using System.Collections;

public class AccusationFx : MonoBehaviour
{
	public SpriteRenderer LightningSprite;
	public SpriteRenderer[] TrainDarkness;

	public IEnumerator DoAccusationFx()
	{
		yield return new WaitForSeconds(0.3f);

		audio.Play ();
		yield return new WaitForSeconds(0.25f);

		foreach(SpriteRenderer darkness in TrainDarkness)
		{
			darkness.gameObject.SetActive(true);
		}

		LightningSprite.gameObject.SetActive(true);
		yield return new WaitForSeconds(0.05f);
		LightningSprite.gameObject.SetActive(false);
		yield return new WaitForSeconds(0.1f);
		LightningSprite.gameObject.SetActive(true);
		yield return new WaitForSeconds(0.2f);
		LightningSprite.gameObject.SetActive(false);

		yield return new WaitForSeconds(1f);

		foreach(SpriteRenderer darkness in TrainDarkness)
		{
			darkness.gameObject.SetActive(false);
		}
	}
}
