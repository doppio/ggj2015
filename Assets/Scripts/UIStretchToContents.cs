﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIStretchToContents : MonoBehaviour {
	public UIWidget Widget;
	public GameObject Contents;
	public List<UIWidget> IgnoreList;
	public Vector2 Padding;

	public bool RefreshNow { get; set; }

	void Start()
	{
		Refresh();
	}

	void Update()
	{
		if(RefreshNow)
		{
			Refresh ();
			RefreshNow = false;
		}
	}
	
	public void Refresh ()
	{
		List<UIWidget> deactivatedWidgets = new List<UIWidget>();
		foreach(UIWidget widget in IgnoreList)
		{
			if(widget.gameObject.activeSelf)
			{
				widget.gameObject.SetActive(false);
				deactivatedWidgets.Add (widget);
			}
		}

		Bounds bounds = NGUIMath.CalculateRelativeWidgetBounds(Widget.transform, Contents.transform, false);
		Widget.width = Mathf.RoundToInt(bounds.size.x + Padding.x);
		Widget.height = Mathf.RoundToInt(bounds.size.y + Padding.y);

		foreach(UIWidget deactivatedWidget in deactivatedWidgets)
		{
			deactivatedWidget.gameObject.SetActive(true);
		}
	}
}
