﻿using UnityEngine;
using System.Collections;

public class ScrollingBackground : MonoBehaviour
{
	public static float MagicNumber = 0.3f; // Totally a sign of good code

	public static float SpeedMultiplier = 1f;
	public float Speed;
	public TrainCars TrainCars;

	private Vector3 m_LastTrainsPosition;

	void LateUpdate()
	{
		float additionalScroll = 0f;
		if(!TrainCars.MovingOffScreen)
		{
			Vector3 newTrainsPosition = TrainCars.transform.position;
			float difference = newTrainsPosition.x - m_LastTrainsPosition.x;
			additionalScroll = -difference * Speed * MagicNumber;
			m_LastTrainsPosition = newTrainsPosition;
		}

		Vector2 textureOffset = renderer.material.mainTextureOffset;
		textureOffset.x += Speed * Time.deltaTime * SpeedMultiplier + additionalScroll;
		renderer.material.mainTextureOffset = textureOffset;
	}
}
