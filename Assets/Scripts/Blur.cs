﻿using UnityEngine;
using System.Collections;

public class Blur : MonoBehaviour {
	public float Multiplier = 0.3f;

	private MotionBlur m_MotionBlur;

	void Awake()
	{
		m_MotionBlur = GetComponent<MotionBlur>();
	}

	void Update()
	{
		m_MotionBlur.blurAmount = (ScrollingBackground.SpeedMultiplier - 1) * Multiplier;
	}
}
