﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {
	private static GameManager m_Instance;
	public static GameManager Instance
	{
		get { return m_Instance; }
	}

	public int MaxNumRounds = 5;
	public float TimeScaleIncrement = 0.5f;
	public AccusationFx AccusationFx;
	public AudioClip LoseSound;
	public GameObject ReplayButton;
	public TrainCars TrainCars;
	public Camera ForegroundCamera;

	private Character[] m_AllCharacters;
	private List<Character> m_LivingCharacters;
	private int m_CurrentRound = 0;
	
	private Character m_PossessedCharacter;
	public Character PossessedCharacter { get { return m_PossessedCharacter; } }

	private bool m_Started;
	public bool Started { get { return m_Started; } }

	private bool m_PlayingSequence;

	public bool InteractionsPermitted { get { return m_Started && !m_PlayingSequence && !SystemNotification.Instance.Showing; } }

	public float TimeScale { get; set; }

	void Awake()
	{
		m_Instance = this;
		TimeScale = 1f;
	}

	void Start()
	{
		m_AllCharacters = FindObjectsOfType<Character>();

		m_LivingCharacters = new List<Character>();
		foreach(Character character in m_AllCharacters)
			m_LivingCharacters.Add (character);

		LoadRound(m_CurrentRound);
	}

	void Update()
	{
		Time.timeScale = TimeScale;
	}

	public void StartGame()
	{
		StartCoroutine (WaitAndStart());
	}

	private IEnumerator WaitAndStart()
	{
		yield return new WaitForSeconds(0.25f);

		SystemNotification.Instance.PushMessage("This haunted train has lost control! Each round, the ghost will possess a new passenger.", 5f);
		SystemNotification.Instance.PushMessage("Drag left and right to see all the train cars.", 3f);
		SystemNotification.Instance.PushMessage("By learning about each passenger, can you catch the ghost's lies and exorcise it?", 5f);
		SystemNotification.Instance.PushMessage("BE WARNED: If you exorcise the wrong passenger, they will become a [84F27E]soulless husk[-], and the ghost will [C91113]KILL[-] its real victim.", 7f);

		while(SystemNotification.Instance.Showing)
			yield return null;

		yield return new WaitForSeconds(0.65f);

		TrainCars.TrainDragger.SetActive(true);
		m_Started = true;

		ForegroundCamera.depth = 4;
		
		foreach(Character character in m_AllCharacters)
			character.SayStartingConvo();
	}

	public void Accuse()
	{
		Character speaker = UISpeechBubble.Instance.CurrentSpeaker;
		UISpeechBubble.Instance.Hide();
		StartCoroutine (DoAccusationSequence(speaker));
	}

	private IEnumerator DoAccusationSequence(Character target)
	{
		m_PlayingSequence = true;

		yield return StartCoroutine(AccusationFx.DoAccusationFx());

		string charNameColor = "[" + NGUIText.EncodeColor(SystemNotification.Instance.CharacterNameColor) + "]";
		if(target == m_PossessedCharacter)
		{
			// Success! The player wins.
			string message = "You successfully exorcised the ghost from " + charNameColor + target.Name + "[-] and saved the train! Well done.\n\nThank you for riding The Exorcism Express...";
			Go.to (this, 2f, new GoTweenConfig().floatProp("TimeScale", 0f)).timeScale = 1f;
			TrainCars.Stop();
			SystemNotification.Instance.PushMessage(message);
			ReplayButton.SetActive(true);
		}
		else
		{
			// Failure!
			Exorcise (target);
			Kill(m_PossessedCharacter);
			AudioPlayer.Instance.Play (LoseSound);

			string message = "You wrongly exorcised poor " + charNameColor + target.Name + "[-]. The ghost murdered its real host, " + charNameColor + m_PossessedCharacter.Name + "[-].";

			bool totalFailure = false;
			m_CurrentRound++;
			if(m_CurrentRound >= MaxNumRounds)
			{
				// Total failure. Player just lost the game.
				message += "\n\nThe ghost has killed them all. You've failed.";
				totalFailure = true;

				ScrollingBackground.SpeedMultiplier += 1f;

				foreach(Character character in m_LivingCharacters.ToArray())
					Kill (character);

				SystemNotification.Instance.PushMessage(message);

				yield return new WaitForSeconds(3.5f);
				TrainCars.GoOffScreen();
				m_Started = false;
			}
			else
			{
				SystemNotification.Instance.PushMessage(message);
				LoadRound(m_CurrentRound);
			}

			while(SystemNotification.Instance.Showing)
				yield return null;

			if(totalFailure)
				ReplayButton.SetActive(true);
			else
			{
				SystemNotification.Instance.PushMessage ("The ghost has possessed another passenger.", 2.5f);
			}

			m_PlayingSequence = false;
		}
	}

	public void Replay()
	{
		Application.LoadLevel(0);
	}

	private void LoadRound(int round)
	{
		ScrollingBackground.SpeedMultiplier = 1f + round * TimeScaleIncrement;

		int possessedCharacterIndex = Random.Range(0, m_LivingCharacters.Count);
		for(int i = 0; i < m_LivingCharacters.Count; ++i)
		{
			m_LivingCharacters[i].LoadRound(round);
		}

		m_PossessedCharacter = m_LivingCharacters[possessedCharacterIndex];
	}

	private void Kill(Character character)
	{
		character.Die();
		m_LivingCharacters.Remove(character);
	}

	private void Exorcise(Character character)
	{
		character.Exorcise();
		m_LivingCharacters.Remove(character);
	}

	public Character GetCharacterByName(string name)
	{
		foreach(Character character in m_LivingCharacters)
		{
			if(character.Name == name)
				return character;
		}

		return null;
	}
}
