﻿using UnityEngine;
using System.Collections;

public class AudioPlayer : MonoBehaviour {
	private static AudioPlayer m_Instance;
	public static AudioPlayer Instance
	{
		get { return m_Instance; }
	}
	
	void Awake()
	{
		m_Instance = this;
	}

	public void Play(AudioClip clip, float volume = 1f)
	{
		audio.PlayOneShot(clip, volume);
	}
}
